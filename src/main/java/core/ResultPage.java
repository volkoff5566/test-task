package core;

import core.BasePage;
import core.LoginPage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class ResultPage extends BasePage {
    private By userName = By.xpath("(//div[@class='header_user_info'])[1]");
    private By logOutButton = By.xpath("//a[@class='logout']/..");

    public ResultPage(WebDriver driver) {
        super(driver);
    }

    public String checkUserName() {
        isElementVisible(userName);
        return getText(userName);
    }

    public LoginPage logOutUI() {
        click(logOutButton);
        return new LoginPage(driver);
    }
}
