package core;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class LoginPage extends BasePage{
    private By emailInput = By.xpath("//form[@id='login_form']//input[@data-validate='isEmail']");
    private By passInput = By.xpath("//form[@id='login_form']//input[@data-validate='isPasswd']");
    private By authHeader = By.xpath("//h1[@class='page-heading']");
    private By userName = By.xpath("(//div[@class='header_user_info'])[1]");
    private By submit = By.xpath("//button[@id='SubmitLogin']");

    public LoginPage(WebDriver driver) {
        super(driver);
    }

    public void enterUserCredentials(String email, String pass) {
        isElementVisible(emailInput);
        find(emailInput).sendKeys(email);
        find(passInput).sendKeys(pass);
    }

    public String checkAuthHeader() {
        isElementVisible(authHeader);
        return getText(authHeader);
    }

    public boolean checkUserNameIsNotVisible(String username) {
        return isElementInvisible(userName, username, 10);
    }

    public ResultPage getResultPage() {
        isElementVisible(submit);
        click(submit);
        return new ResultPage(driver);
    }
}
