package com.automation;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.annotations.*;

import java.net.MalformedURLException;
import java.net.URL;

public class BaseTest {
    private String driverType = System.getProperty("driver");
    private WebDriver driver;

    @Parameters({"port", "browser"})
    @BeforeClass
    public void initiateDriver(@Optional("9001") String port, String browser) throws MalformedURLException {
        if (driverType.equals("remote")) {
            if (port.equalsIgnoreCase("9001")) {
                driver = new RemoteWebDriver(new URL("http://docker:4444/wd/hub"), DesiredCapabilities.chrome());
            } else if (port.equalsIgnoreCase("9002")) {
                driver = new RemoteWebDriver(new URL("http://docker:4444/wd/hub"), DesiredCapabilities.firefox());
            }
        } else if (driverType.equals("local")) {
            if (browser.equals("chrome")) {
                ChromeOptions options = new ChromeOptions();
                options.addArguments("--disable-notifications");
                System.setProperty("webdriver.chrome.driver", "src/main/resources/chromedriver.exe");
                driver = new ChromeDriver(options);
            } else if (browser.equals("firefox")) {
                System.setProperty("webdriver.gecko.driver", "src/main/resources/geckodriver.exe");
                driver = new FirefoxDriver();
            }
        }
        driver.manage().window().maximize();
    }

    public WebDriver getDriver() {
        return this.driver;
    }

    public void open(String url) {
        driver.get(url);
    }

    @AfterClass
    public void threadDown() {
        if (driver != null) {
            driver.quit();
        }
    }
}
