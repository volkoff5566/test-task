package com.automation;

import core.LoginPage;
import core.ResultPage;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

public class TestClass extends BaseTest {
    private WebDriver driver;
    private ResultPage resultPage;
    private LoginPage loginPage;
    private final String BASE_URL = "http://automationpractice.com/index.php?controller=authentication&back=my-account";
    private final String LOGOUT_URL = "http://automationpractice.com/index.php?mylogout=";
    // all credentials should be moved to .property file or passed through command line
    private String email = "volkoff5566@gmail.com";
    private String pass = "12345";
    private String username = "John Dou";


    @BeforeClass
    public void setUp() {
        driver = getDriver();
    }

    @BeforeMethod
    public void login() {
        open(BASE_URL);
        loginPage = new LoginPage(driver);
        loginPage.enterUserCredentials(email, pass);
        resultPage = loginPage.getResultPage();
        assertEquals(resultPage.checkUserName(), username);
    }

    @Test(priority = 1)
    public void logOutByUI() {
        resultPage.logOutUI();
        assertEquals(loginPage.checkAuthHeader(), "AUTHENTICATION");
    }

    @Test(priority = 2)
    public void logOutByLink() {
        open(LOGOUT_URL);
        assertTrue(loginPage.checkUserNameIsNotVisible(username));
    }
}
